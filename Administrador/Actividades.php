<?php
require "../Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(is_null($guardia)){
    header('Location: ../Login.php');  
  }
  else if($guardia['status']!=2){
    header('Location: ../Guardia/Escaner.php');
  }
} else {
  header('Location: ../Login.php');
}

if (isset($_POST['search']))
{
  $value = $_POST['valueToSearch'];
  $sql = "SELECT * FROM actividad WHERE CONCAT(id, idGuardia, idPueEntr, idPueSal,
    idPersona, horaEntrada, horaSalida) LIKE '%" . $value . "%'";
    $result = Filter($sql);
  }
  else
  {
    $sql = "SELECT * FROM actividad LIMIT 0, 300 ";
    $result = Filter($sql);
  }

  function Filter($sql)
  {
    $con = conecta();
    $result = mysqli_query($con, $sql);
    mysqli_close($con);
    return $result;
  }
  ?>

  <html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="../CSS/style.css" type="text/css">
    <meta charset="utf-8">
    <title>Administrador</title>
    <script type="text/javascript" src="Administrador.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>

    <div class="tab">
      <button class="tablinks tabButtonActive" onclick="document.location.href='Actividades.php'">Actividades</button>
      <button class="tablinks" onclick="document.location.href='Visitantes.php'"> Visitantes</button>
      <button class="tablinks" onclick="document.location.href='Residentes.php'">Residentes</button>
      <button class="tablinks" onclick="document.location.href='Guardias.php'">Guardias</button>
      <button class="tablinks" onclick="document.location.href='Puertas.php'">Puertas</button>
    </div>

    <center>

      <br>

      <div class="dropdown">
        <span class="button" style="float: left">Click!</span>
        <br>
        <br>
        <div class="dropdown-content">
          <a class="button darkGrey lightYellowText" href="#openVisPen"
          style="float: left">Salidas Pendientes</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText" href="#openResPen"
          style="float: left">Residentes Dentro</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText a" href="../CerrarSesion.php"
          style="float: left; width: 70%">Cerrar Sesion</a>
        </div>
      </div>

      <form action="Actividades.php" method="post">
        <input type="text" name="valueToSearch" placeholder="Buscar...">
        <button class="button" type="submit" name="search">Go!</button>
      </form>
    </div>
    <br>

    <table class="DarkTable">
      <tr>
        <!--<th colspan="2"span>Usuario</th>-->
        <th colspan="2">Usuario</th>
        <th>Guardia</th>
        <th>Entrada</th>
        <th>Puerta Entrada</th>
        <th>Salida</th>
        <th>Puerta Salida</th>
      </tr>
      <?php
      if ($result->num_rows)
      {
        $mesagge = "";
        $conec = conecta();
        for ($i = 0; $i < $result->num_rows; $i++)
        {
          $fila = $result->fetch_assoc();
          $id = $fila['id'];
          $idPer     = $fila['idPersona'];
          $idGuar    = $fila['idGuardia'];
          $idPueEnt  = $fila['idPueEtr'];
          $idPueSal  = $fila['idPueSal'];
          $entrada   = $fila['horaEntrada'];
          $salida    = $fila['horaSalida'];

          $sqlVIS    = "SELECT visitante FROM persona WHERE id=$idPer";
          $visitante = mysqli_fetch_assoc(mysqli_query($conec, $sqlVIS))['visitante'];
          /*$visitante = mysqli_query($conec, $sqlVIS);
          $resultVis = $visitante->fetch_assoc();
          $tipo = $resultVis['visitante'];
          */
          if ($visitante == 0)
          {
            $visitante = 'Residente';
          }
          else
          {
            $visitante = 'Visitante';
          }
          if($salida=='0000-00-00 00:00:00'){
            $salida = 'No ha salido';
          }

          echo '<tr>
          <td>' . $visitante . '</td>
          <td>' . $idPer . '</td>
          <td>' . $idGuar . '</td>
          <td>' . $entrada . '</td>
          <td>' . $idPueEnt . '</td>
          <td>' . $salida . '</td>
          <td>' . $idPueSal . '</td>
          </tr>';
        }
      }
      else
      {
        $mesagge = "No hay Resultados para mostrar";
      }
      ?>
    </table>
    <label style="color:#ffdd00"><?php echo $mesagge; ?></label>
  </center>

  <div id="openVisPen" class="modalWindow">
    <div>
      <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
      <br>
      <h2 style="color:white">Visitantes Dentro</h2>
      <?php
      $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
      ON (a.idPersona = p.id) WHERE p.visitante=1
      AND a.horaSalida=0 OR a.horaSalida = NULL";
      $resultVis= Filter($sql);
      $conn = conecta();
      $resultVis = mysqli_query($conn, $sql);
      ?>
      <center>
        <br>
        <table class="DarkTable" style="width: 90%">
          <tr>
            <th colspan="2">Usuario</th>
            <th>Guardia</th>
            <th>Puerta</th>
            <th>Entrada</th>
          </tr>
          <?php
          if ($resultVis->num_rows)
          {
            for ($i = 0; $i < $resultVis->num_rows; $i++)
            {
              $mesagge = "";
              $fila = $resultVis->fetch_assoc();
              $id       = $fila['id'];
              $idPer    = $fila['idPersona'];
              $idGuar   = $fila['idGuardia'];
              $idPueEnt = $fila['idPueEtr'];
              $entrada  = $fila['horaEntrada'];


              echo '<tr>
              <td>Visitante</td>
              <td>' . $idPer . '</td>
              <td>' . $idGuar . '</td>
              <td>' . $idPueEnt . '</td>
              <td>' . $entrada . '</td>
              </tr>';
            }
          }
          else
          {
            $mesagge = "No hay Resultados para mostrar";
          }
          mysqli_close($conn);
          ?>
        </table>
        <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
      </center>
    </div>
  </div>

  <div id="openResPen" class="modalWindow">
    <div>
      <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
      <br>
      <h2 style="color:white">Residentes Dentro</h2>
      <?php
      $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
      ON (a.idPersona = p.id) WHERE p.visitante=0
      AND a.horaSalida=0 OR a.horaSalida = NULL";
      $resultVis= Filter($sql);
      $conn = conecta();
      $resultVis = mysqli_query($conn, $sql);
      ?>
      <center>
        <br>
        <table class="DarkTable" style="width: 90%">
          <tr>
            <th colspan="2">Usuario</th>
            <th>Guardia</th>
            <th>Puerta</th>
            <th>Entrada</th>
          </tr>
          <?php
          if ($resultVis->num_rows)
          {
            for ($i = 0; $i < $resultVis->num_rows; $i++)
            {
              $mesagge = "";
              $fila = $resultVis->fetch_assoc();
              $id       = $fila['id'];
              $idPer    = $fila['idPersona'];
              $idGuar   = $fila['idGuardia'];
              $idPueEnt = $fila['idPueEtr'];
              $entrada  = $fila['horaEntrada'];


              echo '<tr>
              <td>Residente</td>
              <td>' . $idPer . '</td>
              <td>' . $idGuar . '</td>
              <td>' . $idPueEnt . '</td>
              <td>' . $entrada . '</td>
              </tr>';
            }
          }
          else
          {
            $mesagge = "No hay Resultados para mostrar";
          }
          mysqli_close($conn);
          ?>
        </table>
        <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
      </center>
    </div>
  </div>
</body>
</html>
