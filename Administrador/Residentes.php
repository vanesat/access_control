<?php
require "../Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(is_null($guardia)){
    header('Location: ../Login.php');  
  }
  else if($guardia['status']!=2){
    header('Location: ../Guardia/Escaner.php');
  }
} else {
  header('Location: ../Login.php');
}

if (isset($_POST['search']))
{
  $value = $_POST['valueToSearch'];
  $sql = "SELECT * FROM persona WHERE CONCAT(id, nombre, vehiculoDescrip,
    placas, correo)
    LIKE '%" . $value . "%'" . "AND visitante = 0 AND status = 1";
    $result = Filter($sql);
  }
  elseif (isset($_POST['update']))
  {
    $id       = $_POST['id'];
    $correo   = $_POST['correo'];
    $nombre   = $_POST['nombre'];
    $vehiculo = $_POST['vehiculo'];
    $placas   = $_POST['placas'];

    $sql = "UPDATE persona SET nombre = '$nombre', vehiculoDescrip = '$vehiculo',
    placas = '$placas',
    correo = '$correo' WHERE id = $id";
    $conec = conecta();
    mysqli_query($conec, $sql);

    $result = mysqli_query($conec, "SELECT * FROM persona WHERE visitante = 0 AND status = 1");
  }
  elseif (isset($_POST['add']))
  {
    $nombre   = $_POST['nombreA'];
    $correo   = $_POST['correoA'];
    $foto     = PATHINFO($_FILES["fotoA"]["name"]);
    $vehiculo = $_POST['vehiculoA'];
    $placas   = $_POST['placasA'];
    //$codBar   = $_POST['codBarA'];

    $newFilename = $foto['filename'] ."_". time() . "." . $foto['extension'];
    move_uploaded_file($_FILES["fotoA"]["tmp_name"],"Fotos/" . $newFilename);

    $sql = "INSERT INTO persona (nombre, correo, foto, vehiculoDescrip, placas, visitante, status)
    VALUES ('$nombre', '$correo', '$newFilename', '$vehiculo', '$placas', 0, 1)";
    $conec = conecta();
    mysqli_query($conec, $sql);
    //var_dump($sql);

    $result = mysqli_query($conec, "SELECT * FROM persona WHERE visitante = 0 AND status = 1");
  }
  else
  {
    $sql = "SELECT * FROM persona WHERE visitante = 0 AND status = 1";
    $result = Filter($sql);
  }

  function Filter($sql)
  {
    $con = conecta();
    $result = mysqli_query($con, $sql);
    return $result;
  }
  ?>

  <html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Administrador</title>
    <script type="text/javascript" src="Administrador.js"></script>
    <link rel="stylesheet" href="../CSS/style.css" type="text/css">
  </head>

  <body>

    <div class="tab">
      <button class="tablinks" onclick="document.location.href='Actividades.php'">Actividades</button>
      <button class="tablinks" onclick="document.location.href='Visitantes.php'">Visitantes</button>
      <button class="tablinks tabButtonActive" onclick="document.location.href='Residentes.php'">Residentes</button>
      <button class="tablinks" onclick="document.location.href='Guardias.php'">Guardias</button>
      <button class="tablinks" onclick="document.location.href='Puertas.php'">Puertas</button>
    </div>


    <center>

      <br>
      <div class="dropdown">
        <span class="button" style="float: left">Click!</span>
        <br>
        <br>
        <div class="dropdown-content">
          <a class="button darkGrey lightYellowText" href="#openVisPen"
          style="float: left">Salidas Pendientes</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText" href="#openResPen"
          style="float: left">Residentes Dentro</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText" href="#openAdd"
          style="float: left">Agregar Residente</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText a" href="../CerrarSesion.php"
          style="float: left; width: 70%">Cerrar Sesion</a>
        </div>
      </div>

      <form action="Residentes.php" method="post">
        <input type="text" name="valueToSearch" placeholder="Buscar...">
        <button class="button" type="submit" name="search">Go!</button>
      </form>

      <br>
      <table class="DarkTable">
        <tr>
          <th width=15%>Foto</th>
          <th colspan="2">Identificacion</th>
          <th width=15%>Correo</th>
          <th width=25% colspan="2">Vehiculo</th>
          <th width=13% class="grey"></th>
        </tr>
        <?php
        if ($result->num_rows)
        {
          $mesagge = "";
          for ($i = 0; $i < $result->num_rows; $i++)
          {
            $fila     = $result->fetch_assoc();
            $id       = $fila['id'];
            $foto     = $fila['foto'];
            $correo   = $fila['correo'];
            $nombre   = $fila['nombre'];
            $vehiculo = $fila['vehiculoDescrip'];
            $placas   = $fila['placas'];

            echo '<tr>
            <td style="width:15%"><img width="100%" height="9%" src="Fotos/' . $foto . '"></td>
            <td style="width:3%">' . $id . '</td>
            <td style="width:20%">' . $nombre . '</td>
            <td style="width:15%">' . $correo . '</td>
            <td style="width:10%">' . $placas . '</td>
            <td style="width:15%">' . $vehiculo . '</td>
            <td>
            <button class="button" onclick="borrarInstancia(' . $id . ', \'persona\')">B</button>
            <a class="button" href="#openModal'.$i.'" style="float: right">E</a>

            <div id="openModal'.$i.'" class="modalWindow">
            <div style="width: 30%">
            <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
            <form action="Residentes.php" method="post">
            <input type="hidden" name="id" value="' . $id . '">
            <ul class="formUpdate" style="list-style-type: none; width: 100%">
            <li>
            <img src=Fotos/"' . $foto . '">
            </li>
            <br>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="' . $nombre . '">
            </li>
            <li>
            <br>
            <label for="correo">Correo</label>
            <input type="email" name="correo" value="' . $correo . '">
            </li>
            <li>
            <br>
            <label for="placas">Placas</label>
            <input type="text" name="placas" value="' . $placas . '">
            </li>
            <li>
            <br>
            <label for="vehiculo">Descripcion vehiculo</label>
            <br>
            <textarea type="text" name="vehiculo" value="' . $vehiculo . '"></textarea>
            </li>
            <li>
            <br>
            <button class="button" type="submit" name="update">Go!</button>
            </li>
            </ul>
            </form>
            </div>
            </div>
            </td>
            </tr>';
          }
        }
        else
        {
          $mesagge = "No hay Resultados para mostrar";
        }
        ?>

      </table>
      <label class="yellowText"><?php echo $mesagge; ?></label>
    </center>

    <!--#################################Dropdown################################-->
    <!--#################################MENU####################################-->
    <!--#################################Modals##################################-->
    <div id="openVisPen" class="modalWindow">
      <div>
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <br>
        <h2 style="color:white">Visitantes Dentro</h2>
        <?php
        $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
        ON (a.idPersona = p.id) WHERE p.visitante=1 AND a.horaSalida=0 OR a.horaSalida = NULL";
        $resultVis= Filter($sql);
        $conn = conecta();
        $resultVis = mysqli_query($conn, $sql);
        ?>
        <center>
          <br>
          <table class="DarkTable" style="width: 90%">
            <tr>
              <th colspan="2"span>Usuario</th>
              <th>Guardia</th>
              <th>Puerta</th>
              <th>Entrada</th>
            </tr>
            <?php
            if ($resultVis->num_rows)
            {
              for ($i = 0; $i < $resultVis->num_rows; $i++)
              {
                $mesagge = "";
                $fila = $resultVis->fetch_assoc();
                $id       = $fila['id'];
                $idPer    = $fila['idPersona'];
                $idGuar   = $fila['idGuardia'];
                $idPueEnt = $fila['idPueEnt'];
                $entrada  = $fila['horaEntrada'];


                echo '<tr>
                <td>' . $visitante . '</td>
                <td>' . $idPer . '</td>
                <td>' . $idGuar . '</td>
                <td>' . $idPueEnt . '</td>
                <td>' . $entrada . '</td>
                </tr>';
              }
            }
            else
            {
              $mesagge = "No hay Resultados para mostrar";
            }
            mysqli_close($conn);
            ?>
          </table>
          <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
        </center>
      </div>
    </div>

    <div id="openResPen" class="modalWindow">
      <div>
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <br>
        <h2 style="color:white">Residentes Dentro</h2>
        <?php
        $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
        ON (a.idPersona = p.id) WHERE p.visitante=0 AND a.horaSalida=0 OR a.horaSalida = NULL";
        $resultVis= Filter($sql);
        $conn = conecta();
        $resultVis = mysqli_query($conn, $sql);
        ?>
        <center>
          <br>
          <table class="DarkTable" style="width: 90%">
            <tr>
              <th colspan="2"span>Usuario</th>
              <th>Guardia</th>
              <th>Puerta</th>
              <th>Entrada</th>
            </tr>
            <?php
            if ($resultVis->num_rows)
            {
              for ($i = 0; $i < $resultVis->num_rows; $i++)
              {
                $mesagge = "";
                $fila = $resultVis->fetch_assoc();
                $id       = $fila['id'];
                $idPer    = $fila['idPersona'];
                $idGuar   = $fila['idGuardia'];
                $idPueEnt = $fila['idPueEnt'];
                $entrada  = $fila['horaEntrada'];


                echo '<tr>
                <td>' . $visitante . '</td>
                <td>' . $idPer . '</td>
                <td>' . $idGuar . '</td>
                <td>' . $idPueEnt . '</td>
                <td>' . $entrada . '</td>
                </tr>';
              }
            }
            else
            {
              $mesagge = "No hay Resultados para mostrar";
            }
            mysqli_close($conn);
            ?>
          </table>
          <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
        </center>
      </div>
    </div>

    <div id="openAdd" class="modalWindow">
      <div style="width: 30%">
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <form action="Residentes.php" method="post" enctype="multipart/form-data">
          <ul class="formUpdate" style="list-style-type: none; width: 100%">
            <li>
              <label for="fotoA">Foto</label>
              <input type="file" name="fotoA" accept=".png, .jpg, .jpeg" required>
            </li>
            <br>
            <li>
              <label for="nombre">Nombre</label>
              <input type="text" name="nombreA" required>
            </li>
            <li>
              <br>
              <label for="correo">Correo</label>
              <input type="email" name="correoA" required>
            </li>
            <li>
              <br>
              <label for="placas">Placas</label>
              <input type="text" name="placasA">
            </li>
            <li>
              <br>
              <label for="vehiculo">Descripcion vehiculo</label>
              <br>
              <textarea type="text" name="vehiculoA"></textarea>
            </li>
            <li>
              <br>
              <button class="button" type="submit" name="add">Go!</button>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </body>
  </html>
