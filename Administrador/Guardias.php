<?php
require "../Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(is_null($guardia)){
    header('Location: ../Login.php');  
  }
  else if($guardia['status']!=2){
    header('Location: ../Guardia/Escaner.php');
  }
} else {
  header('Location: ../Login.php');
}

if (isset($_POST['search']))
{
  $value = $_POST['valueToSearch'];
  $sql = "SELECT * FROM guardia WHERE CONCAT(id, nombre, correo)
  LIKE '%" . $value . "%'" . "AND status = 1";
  $result = Filter($sql);
}
elseif (isset($_POST['update']))
{
  $id       = $_POST['id'];
  $correo   = $_POST['correo'];
  $nombre   = $_POST['nombre'];
  $pass   = $_POST['pass'];
  $sql = "UPDATE guardia SET nombre = '$nombre', correo = '$correo', password = '$pass' WHERE id = $id";
  $conec = conecta();
  mysqli_query($conec, $sql);

  $result = mysqli_query($conec,"SELECT * FROM guardia WHERE status = 1");
}
elseif (isset($_POST['add']))
{
  $nombre = $_POST['nombreA'];
  $correo = $_POST['correoA'];
  $pass   = $_POST['passA'];


  $sql = "INSERT INTO guardia (nombre, correo, password, status) VALUES ('$nombre', '$correo', '$pass', 1)";
  $conec = conecta();
  mysqli_query($conec, $sql);

  $result = mysqli_query($conec, "SELECT * FROM guardia WHERE status = 1");
}
else
{
  $sql = "SELECT * FROM guardia WHERE status = 1";
  $result = Filter($sql);
}

function Filter($sql)
{
  $con = conecta();
  $result = mysqli_query($con,$sql);
  return $result;
}
?>

<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Administrador</title>
  <script type="text/javascript" src="Administrador.js"></script>
  <link rel="stylesheet" href="../CSS/style.css" type="text/css">
</head>

<body>

  <div class="tab">
    <button class="tablinks" onclick="document.location.href='Actividades.php'">
      Actividades</button>
      <button class="tablinks "
      onclick="document.location.href='Visitantes.php'">Visitantes</button>
      <button class="tablinks"
      onclick="document.location.href='Residentes.php'">Residentes</button>
      <button class="tablinks tabButtonActive"
      onclick="document.location.href='Guardias.php'">Guardias</button>
      <button class="tablinks"
      onclick="document.location.href='Puertas.php'">Puertas</button>
    </div>


    <center>

      <br>
      <div class="dropdown">
        <span class="button" style="float: left">Click!</span>
        <br>
        <br>
        <div class="dropdown-content">
          <a class="button darkGrey lightYellowText" href="#openVisPen"
          style="float: left">Salidas Pendientes</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText" href="#openResPen"
          style="float: left">Residentes Dentro</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText" href="#openAdd"
          style="float: left">Agregar Guardia</a>
          <br>
          <br>
          <br>
          <a class="button darkGrey lightYellowText a" href="../CerrarSesion.php"
          style="float: left; width: 70%">Cerrar Sesion</a>
        </div>
      </div>

      <form action="Guardias.php" method="post">
        <input type="text" name="valueToSearch" placeholder="Buscar...">
        <button class="button" type="submit" name="search">Go!</button>
      </form>
      <br>

      <table class="DarkTable" style="60%">
        <tr>
          <th colspan="2"span>Identificacion</th>
          <th style="width:15%">Correo</th>
          <th class="grey" width=5%></th>
        </tr>
        <?php
        if ($result->num_rows)
        {
          $mesagge = " ";
          for ($i = 0; $i < $result->num_rows; $i++)
          {
            $fila = $result->fetch_assoc();
            $correo = $fila['correo'];
            $pass   = $fila['password'];
            $nombre = $fila['nombre'];
            $id     = $fila['id'];

            echo '<tr>
            <td style="width:3%">' . $id . '</td>
            <td style="width:25%">' . $nombre . '</td>
            <td>' . $correo . '</td>
            <td>
            <button class="button" onclick="borrarInstancia(' . $id . ', \'guardia\')">B</button>
            <a class="button" href="#openModal'.$i.'" style="float: right">E</a>

            <div id="openModal'.$i.'" class="modalWindow">
            <div style="width: 30%">
            <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
            <form action="Guardias.php" method="post">
            <input type="hidden" name="id" value="' . $id . '">
            <ul class="formUpdate" style="list-style-type: none; width: 100%">
            <li>
            <br>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="' . $nombre . '">
            </li>
            <li>
            <br>
            <label for="correo">Correo</label>
            <input type="email" name="correo" value="' . $correo . '">
            </li>
            <li>
            <br>
            <label for="pass">Contraseña</label>
            <input type="text" name="pass" value="' . $pass . '">
            </li>
            <br>
            <li>
            <br>
            <button class="button" type="submit" name="update">Go!</button>
            </li>
            </ul>
            </form>
            </div>
            </div>
            </td>
            </tr>';
          }
        }
        else
        {
          $mesagge = "No hay Resultados para mostrar";
        }
        ?>

      </table>

      <label class="yellowText">
        <?php echo $mesagge; ?>
      </label>
    </center>
    <!--#################################Dropdown################################-->
    <!--#################################MENU####################################-->
    <!--#################################Modals##################################-->
    <div id="openVisPen" class="modalWindow">
      <div>
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <br>
        <h2 style="color:white">Visitantes Dentro</h2>
        <?php
        $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
        ON (a.idPersona = p.id) WHERE p.visitante=1
        AND a.horaSalida=0 OR a.horaSalida = NULL";
        $resultVis= Filter($sql);
        $conn = conecta();
        $resultVis = mysqli_query($conn, $sql);
        ?>
        <center>
          <br>
          <table class="DarkTable" style="width: 90%">
            <tr>
              <th colspan="2"span>Usuario</th>
              <th>Guardia</th>
              <th>Puerta</th>
              <th>Entrada</th>
            </tr>
            <?php
            if ($resultVis->num_rows)
            {
              for ($i = 0; $i < $resultVis->num_rows; $i++)
              {
                $mesagge = "";
                $fila = $resultVis->fetch_assoc();
                $id       = $fila['id'];
                $idPer    = $fila['idPersona'];
                $idGuar   = $fila['idGuardia'];
                $idPueEnt = $fila['idPueEnt'];
                $entrada  = $fila['horaEntrada'];


                echo '<tr>
                <td>' . $visitante . '</td>
                <td>' . $idPer . '</td>
                <td>' . $idGuar . '</td>
                <td>' . $idPueEnt . '</td>
                <td>' . $entrada . '</td>
                </tr>';
              }
            }
            else
            {
              $mesagge = "No hay Resultados para mostrar";
            }
            mysqli_close($conn);
            ?>
          </table>
          <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
        </center>
      </div>
    </div>

    <div id="openResPen" class="modalWindow">
      <div>
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <br>
        <h2 style="color:white">Residentes Dentro</h2>
        <?php
        $sql = "SELECT * FROM actividad AS a INNER JOIN persona AS p
        ON (a.idPersona = p.id) WHERE p.visitante=0
        AND a.horaSalida=0 OR a.horaSalida = NULL";
        $resultVis= Filter($sql);
        $conn = conecta();
        $resultVis = mysqli_query($conn, $sql);
        ?>
        <center>
          <br>
          <table class="DarkTable" style="width: 90%">
            <tr>
              <th colspan="2"span>Usuario</th>
              <th>Guardia</th>
              <th>Puerta</th>
              <th>Entrada</th>
            </tr>
            <?php
            if ($resultVis->num_rows)
            {
              for ($i = 0; $i < $resultVis->num_rows; $i++)
              {
                $mesagge = "";
                $fila = $resultVis->fetch_assoc();
                $id       = $fila['id'];
                $idPer    = $fila['idPersona'];
                $idGuar   = $fila['idGuardia'];
                $idPueEnt = $fila['idPueEnt'];
                $entrada  = $fila['horaEntrada'];


                echo '<tr>
                <td>' . $visitante . '</td>
                <td>' . $idPer . '</td>
                <td>' . $idGuar . '</td>
                <td>' . $idPueEnt . '</td>
                <td>' . $entrada . '</td>
                </tr>';
              }
            }
            else
            {
              $mesagge = "No hay Resultados para mostrar";
            }
            mysqli_close($conn);
            ?>
          </table>
          <label style="color:#ffdd00"> <?php echo $mesagge; ?> </label>
        </center>
      </div>
    </div>

    <div id="openAdd" class="modalWindow">
      <div style="width: 30%">
        <a href="#ok" title="Ok" class="ok grey button  yellowText" style="float: right">x</a>
        <form action="Guardias.php" method="post" enctype="multipart/form-data">
          <ul class="formUpdate" style="list-style-type: none; width: 100%">
            <li>
              <br>
              <label for="nombre">Nombre</label>
              <input type="text" name="nombreA" required>
            </li>
            <li>
              <br>
              <label for="correo">Correo</label>
              <input type="email" name="correoA" required>
            </li>
            <li>
              <br>
              <label for="passA">Contraseña</label>
              <input type="password" name="passA" required>
            </li>
            <li>
              <br>
              <button class="button" type="submit" name="add">Go!</button>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </body>
  </html>
