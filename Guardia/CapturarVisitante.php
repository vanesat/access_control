<?php 
    require "../Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(is_null($guardia)){
    header('Location: ../Login.php');  
  }
} else {
  header('Location: ../Login.php');
}

    if(isset($_POST['nombreVis']) && isset($_POST['correoVis'])){

        $nombre   = $_POST['nombreVis'];
        $correo   = $_POST['correoVis'];
        $foto     = PATHINFO($_FILES["fotoA"]["name"]);
        $vehiculo = $_POST['vehiculoDescrip'];
        $placas   = $_POST['placas'];
        //$codBar   = $_POST['codBarA'];
        var_dump($_FILES);
        $newFilename = $foto['filename'] ."_". time() . "." . $foto['extension'];
        move_uploaded_file($_FILES["fotoA"]["tmp_name"],"../Administrador/Fotos/" . $newFilename);

        $sql = "INSERT INTO persona (nombre, correo, foto, vehiculoDescrip, placas, visitante, status)
        VALUES ('$nombre', '$correo', '$newFilename', '$vehiculo', '$placas', 1, 1)";
        $conec = conecta();
        mysqli_query($conec, $sql);
        header('Location: Escaner.php?id='.mysqli_insert_id($conec));
        
        //var_dump($sql);

        //$result = mysqli_query($conec, "SELECT * FROM persona WHERE visitante = 0 AND status = 1");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Guardia - Capturar visitante</title>
	<link rel="stylesheet" type="text/css" href="styles.css?v=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="background-color: rgb(44,51,53);">
	<div class="wrapper margin-top" style="max-width:1400px;">
        <div class="row-padding">
            <div class="third">
            	<p></p>
            </div>
            <div class="third card-4 margin-top" style="padding: 15px; background-color: rgb(21,26,31);">
            	<h2 class="text-yellow">Registrar visitante</h2>
            	<form method="post" name="form" id="form" enctype="multipart/form-data" action="CapturarVisitante.php">
            		<h3 class="text-white" style="font-weight: bold;">Nombre del visitante</h3>
            		<input id="nombreVis" type="text" name="nombreVis" placeholder="Nombre del visitante" value="" required class="text_box">
            		<h3 class="text-white" style="font-weight: bold;">Correo electrónico</h3>
            		<input id="correoVis" name="correoVis" placeholder="Correo electrónico" value="" class="text_box" required>
            		<h3 class="text-white" style="font-weight: bold;">Descripción de vehiculo</h3>
            		<input id="autoDescrip" name="autoDescrip" placeholder="Descripción de vehiculo" value="" class="text_box">
            		<h3 class="text-white" style="font-weight: bold;">Placas</h3>
            		<input id="placas" name="placas" placeholder="Placas" value="" class="text_box">
            		<h3 class="text-white" style="font-weight: bold;">Subir fotografia:</h3>
            		<input type="file" name="fotoA" accept=".png, .jpg, .jpeg" required class="submit" style="font-size: 12px;">
            		<button class="submit" style="width: 100%">Registrar visita</button>
            	</form>
            </div>
        </div>   
    </div>
</body>
</html>