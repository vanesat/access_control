<?php
require "../Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(is_null($guardia)){
    header('Location: ../Login.php');  
  }
} else {
  header('Location: ../Login.php');
}
$error = false;
if(isset($_POST['txtBoxCode'])){
    $sql = "SELECT * FROM persona WHERE id =".$_POST['txtBoxCode'].";";
    $conec = Conecta();
    //$result = mysqli_query($conec, $sql);
    $result = mysqli_fetch_assoc(mysqli_query($conec, $sql));
    if(is_null($result)){
        $error = 1;
    }
    else{
        /*if($result['dentro']){
            // si esta dentro lo va a indicar
            $sql = "UPDATE actividad SET horaSalida =".date('Y-m-d H:i:s')." WHERE idPersona =".$result['id']." AND horaSalida = NULL";
        }*/
        $sql = "SELECT * FROM actividad WHERE idPersona =".$result['id']." ORDER BY id desc LIMIT 1;";
        $entrada = mysqli_fetch_assoc(mysqli_query($conec, $sql));
        //var_dump($entrada);
        if(!$entrada || is_null($entrada)){
            $sql = "INSERT INTO actividad (idGuardia, idPersona, idPueEtr, horaEntrada) VALUES ('".$_COOKIE['currentUser']."','".$result['id']."','".
            $_POST['puerta']."','".date('Y-m-d H:i:s')."')";
            mysqli_query($conec,$sql);
            $sql = "UPDATE persona SET dentro=1 WHERE id=".$result['id'].";";
            mysqli_query($conec,$sql);
        }
        else{
            if($result['dentro'] && $_POST['tipo']=='E'){
                $error = 2;
            }
            else if(!$result['dentro'] && $_POST['tipo']=='S'){
                $error = 3;
            }
            else{
                //var_dump($entrada);
                if($entrada['horaSalida']==NULL){
                    $sql = "UPDATE actividad SET horaSalida='".date('Y-m-d H:i:s')."', idPueSal = ".$_POST['puerta']." WHERE id=".$entrada['id'].";";
                    mysqli_query($conec,$sql);
                    //echo $sql;
                    $sql = "UPDATE persona SET dentro=0 WHERE id=".$result['id'].";";
                    mysqli_query($conec,$sql);
                }
                else{
                    echo 'ENTRE';
                    $sql = "INSERT INTO actividad (idGuardia, idPersona, idPueEtr, horaEntrada) VALUES ('".$_COOKIE[
                    'currentUser']."','".$result['id']."','".$_POST['puerta']."','".date('Y-m-d H:i:s')."')";
                    mysqli_query($conec,$sql);
                    $sql = "UPDATE persona SET dentro=1 WHERE id=".$result['id'].";";
                    mysqli_query($conec,$sql);
                }
            }
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--<link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection"/>-->
    <link rel="stylesheet" type="text/css" href="styles.css?v=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Access_Control - Scanner</title>
</head>
<body style="background-color: rgb(44,51,53);">
	<div class="wrapper margin-top" style="max-width:1400px;">
        <div class="row-padding">
            <div class="third">
                <p></p>
            </div>
            <div class="third">
                <center>
                    <div id="scanner-container"></div>
                    <input type="button" id="btn" value="Activar escaner" class="submit"/>
                    <br>
                    <br>
                    <form id="form" method="POST" action="Escaner.php">
                        <?php 
                            if($error==1){
                                echo '<h4>No existe ese codigo en la base de datos</h4>';
                            }
                            else if($error==2){
                                echo '<h4>Ese usuario esta dentro de las instalaciones</h4>';
                            }
                            else if($error==3){
                                echo '<h4>Ese usuario no esta dentro de las intalaciones</h4>';
                            }
                            if(isset($_GET['id'])){
                                echo '<input type="text" id="txtBoxCode" name="txtBoxCode" class="text_box" value="'.$_GET['id'].'" required>';
                            }
                            else{
                                echo '<input type="text" id="txtBoxCode" name="txtBoxCode" class="text_box" value="" required>';
                            }
                        ?>
                        <br>
                        <br>
                        <div style="font-size: 16px; color: white;">
                            <input type="radio" name="tipo" value="E" checked>E
                            <input type="radio" name="tipo" value="S">S
                            <br>
                            <?php 
                                $conec = conecta();
                                $sqlPuertas = "SELECT * FROM puerta;";
                                $puertas = mysqli_query($conec,$sqlPuertas);
                            ?>
                            <select name="puerta" id="puerta" required>
                                <option value="">Seleccionar puerta</option>
                                <?php
                                    for ($i = 0; $i < $puertas->num_rows; $i++){
                                        $fila = $puertas->fetch_assoc();
                                        echo '<option value="'.$fila['id'].'">'.$fila['direccion'].'</option>';
                                    }
                                ?>
                            </select>
                            <br>
                            <input type="submit" value="Enviar" class="submit">
                        </div>
                    </form>
                    <form action="CapturarVisitante.php">
                        <input type="submit" value="Capturar Visitante" class="submit">
                    </form>
                    <form action="../CerrarSesion.php">
                        <input type="submit" value="Cerrar Sesión" class="submit"/>
                    </form>
                </center>
            </div>
        </div>   
    </div>
    <script src="js/escanerScript.js?v=1"></script>
    <script src="BarCodeReader/quagga.min.js"></script>
</body>
</html>