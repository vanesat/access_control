<?php

require "Coneccion.php";
if(isset($_COOKIE['token']) && isset($_COOKIE['currentUser'])) {
  $token = $_COOKIE['token'];
  $conec = Conecta();
  $sql = "SELECT * FROM Guardia WHERE id=".$_COOKIE['currentUser'].";";
  $guardia = mysqli_fetch_assoc(mysqli_query($conec,$sql));
  if(!is_null($guardia)){
    header('Location: Administrador/Actividades.php');  
  }
}

if(isset($_POST['idGuardia']) && isset($_POST['passwordGuardia'])){
    $usuario=$_POST['idGuardia'];
    $password=$_POST['passwordGuardia'];
    $sql = "SELECT * FROM guardia WHERE password = '$password' AND id = '$usuario';";
    $conec = Conecta();
    $result = mysqli_fetch_assoc(mysqli_query($conec, $sql))['status'];
    if(is_null($result)){
        $error = 1;
    }
    else{
        if($result==0){
            $error = 2;
        }
        else{
            $sql = "UPDATE Guardia SET conectado = 1 WHERE id = '$usuario';";
            mysqli_query($conec,$sql);
            setcookie('token', 'aulsbviavlinasuvcaojfhfr', time() + (86400 * 30), "/");
            setcookie('currentUser', $usuario, time() + (86400 * 30), "/");
            if($result==1){
                header('Location: Guardia/Escaner.php');
            }
            else
                header('Location: Administrador/Actividades.php');
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Guardia - Inicio de sesion</title>
	<link rel="stylesheet" type="text/css" href="styles.css?v=1">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="background-color: rgb(44,51,53);">
	<div class="wrapper margin-top" style="max-width:1400px;">
        <div class="row-padding">
            <div class="third">
            	<p></p>
            </div>
            <div class="third card-4 margin-top" style="padding: 15px; background-color: rgb(21,26,31);">
            	<center><img src="images/logo.png"></center>
            	<form method="post">
                    <?php 
                        if(isset($error) && $error==1){
                            echo "<h2 class='text-white'>Datos incorrectos</h2>";
                        }
                        else if(isset($error) && $error==2){
                            echo "<h2 class='text-white'>Guardia no habilitado</h2>";
                        }
                     ?>
            		<h3 class="text-white" style="font-weight: bold;">ID del guardia</h3>
            		<input id="id_guardia" type="text" name="idGuardia" placeholder="ID del guardia" value="" required class="text_box">
            		<h3 class="text-white" style="font-weight: bold;">Contraseña</h3>
            		<input id="password_guardia" name="passwordGuardia" type="password" placeholder="Contraseña" value="" required class="text_box">
            		<button class="submit" style="width: 100%">Ingresar</button>
            	</form>
            	<p class="text-white">¿Olvidaste la contraseña? Ve con el administrador xD</p>
            </div>
        </div>   
    </div>
</body>
</html>